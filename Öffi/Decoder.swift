//
//  Decoder.swift
//  Öffi
//
//  Created by Johannes Schwinger on 02.06.22.
//

import Foundation

struct MyDecoder {
    func decodeTime(time: String) -> Date? {
        let decoder = DateFormatter()
        decoder.dateFormat = "yyyy-MM-dd'T'HH:mm:sssZ"
        return decoder.date(from: time)
    }
}
