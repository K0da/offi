//
// Created by Johannes Schwinger on 10.06.22.
//

import Foundation

enum FetchError: Error {
    case noSuccessfulConnection(errorCode: Int)
    case noResponse
}
