//
//  O_ffiApp.swift
//  Öffi
//
//  Created by Johannes Schwinger on 02.06.22.
//

import SwiftUI

@main
struct OeffiApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
