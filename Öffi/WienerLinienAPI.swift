//
//  WienerLinienAPI.swift
//  Öffi
//
//  Created by Johannes Schwinger on 02.06.22.
//

import Foundation
import SwiftUI

struct MainView: View {
    let url = URL(string: "http://www.wienerlinien.at/ogd_realtime/monitor?stopId=147&activateTrafficInfo=stoerungkurz&activateTrafficInfo=stoerunglang&activateTrafficInfo=aufzugsinfo")!
    let oldURL = URL(string:"https://api.chucknorris.io/jokes/random")!
    @State private var joke: String = "Standard Text"
    var body: some View {
        Text(joke).textSelection(.enabled)
        Button {
            Task {
                do {
                    let decodedResponse = try await fetchWienerLinien()
                    print("Fetched Data!")
                    joke = decodedResponse.data?.monitors?[0].lines?[0].departures?.departure?[0].departureTime?.timeReal ?? ""
                } catch {
                    print(error)
                    joke = "Error while requesting data from Wiener Linien"
                    return
                }
            }
        } label: {
            Text("Fetch Time")
        }
    }

    func fetchWienerLinien() async throws -> Json4Swift_Base {
        let (data, response) = try await URLSession.shared.data(from: url)
        guard let httpResponse = response as? HTTPURLResponse else {
            throw FetchError.noResponse
        }
        guard httpResponse.statusCode == 200 else {
            throw FetchError.noSuccessfulConnection(errorCode: httpResponse.statusCode)
        }
        return try JSONDecoder().decode(Json4Swift_Base.self, from: data)
    }
}
struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}

